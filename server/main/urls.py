from django.urls import path

from .views import (
    about, contacts, main
)

app_name = 'main'


urlpatterns = [
    path('contacts/', contacts, name='contacts'),
    path('about/', about, name='about'),
    path('', main, name='main'),
]
