from django.template import Template, Context
from django.template.loader import get_template
from django.http import HttpResponse
from django.shortcuts import render


def main(request):
    with open('main/templates/main/index.html') as file:
        template = Template(
            file.read()
        )
        context = Context({
            'name': 'World'
        })
        return HttpResponse(
            template.render(context)
        )
    # return render(request, 'main/index.html')


def about(request):
    template = get_template(
        'main/about.html'
    )
    return HttpResponse(
        template.render({
            'description': 'Информационная страница Интернет-витрины'
        })
    )
    # return render(request, 'main/about.html')


def contacts(request):
    contacts = ['Contact1', 'Contact2','Contact3']

    return render(
        request, 
        'main/contacts.html',
        {'objcts_list': contacts}
    )
